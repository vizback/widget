import React from 'react'
import styled from 'styled-components';

const VizbackButton = styled.div`
    background: red;
    z-index: 2147483644;
    position: fixed;
    right: -2px;
    top: 50%;
    margin-top: -20px;
    bottom: auto;
    border-bottom: 2px solid transparent;
    transform: rotate(-90deg) translateX(50%) translateY(-50%);
    transform-origin: 100% 50%;
    user-select: none;
    text-align: center;
    padding: 12px 16px 8px 16px;
    width: 112px;
    color: white;
    font-family: Arial;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    cursor: pointer;
`


function Widget() {
    return (
        <div>
            <VizbackButton>Feedback</VizbackButton>
        </div>
    )
}

export default Widget
