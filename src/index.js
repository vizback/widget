import React from 'react';
import ReactDOM from 'react-dom';
import Vizback from './vizback'

const root = document.createElement('div')
document.body.appendChild(root)

ReactDOM.render(<Vizback />, root);
